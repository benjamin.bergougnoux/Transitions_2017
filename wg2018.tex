\documentclass[12pt,a4paper]{beamer}
 \usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsfonts,amsthm}
\usepackage{pstricks,pst-tree,pst-node} 
\newcommand*{\dcup}[2]{\displaystyle{\underset{#1}{\overset{#2}{\bigcup}}}}
\beamertemplatenavigationsymbolsempty
\newtheoremstyle{thm}{0cm}{0.35cm}{\itshape}{}{\scshape}{}{\newline}{\underline{\thmname{#1} \thmnumber{#2}} #3}
\theoremstyle{thm}
\newtheorem{thm}{Theorem}
\newcommand*{\dsum}[2]{\displaystyle{\sum_{#1}^{#2}} }

% \usepackage{preambule}
\DeclareMathOperator{\Sep}{Sep}
\DeclareMathOperator{\SEP}{SEP}
\usepackage{color}
\title{On Minimum Connecting Transition Sets in Graphs}
% \date{Thursday, January 12}
\date{Wednesday June 27, 2018}
% \date{}
\author{Thomas \textsc{Bellitto}$^1$ \and Benjamin \textsc{Bergougnoux}$^2$}

% \scritptsize{University of Bordeaux}}
 \usetheme{Darmstadt}

\definecolor{darkblue}{RGB}{0,0,155}
\definecolor{gold}{RGB}{255,215,100}
\definecolor{darkred}{RGB}{155,0,0}
\definecolor{darkgray}{RGB}{75,125,75}
\definecolor{five}{RGB}{215,63,215}

\definecolor{darkgreen}{HTML}{088A08}
\def\blue#1{\textcolor{blue}{{#1}}}
\def\green#1{\textcolor{darkgreen}{{#1}}}
\def\red#1{\textcolor{red}{{#1}}}


\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.25\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}\insertauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\inserttitle
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.25\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
    \usebeamerfont{date in head/foot}\insertshortdate{}\hspace*{2em}
    \insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 
  \end{beamercolorbox}}%
  \vskip0pt%
}

%  \usetheme{Dresden}
% \usecolortheme[named=blue]{structure}
 \addtobeamertemplate{footline}{\hfill\insertframenumber/\inserttotalframenumber}

\begin{document}

 \setbeamertemplate{footline}{} 
% \frame{\titlepage}
\begin{frame}
 \maketitle

\begin{center}
 
\scriptsize{ $1$ - LaBRI, Univ. Bordeaux

\vspace{0.35cm}
$2$ - LIMOS, Univ. Clermont Auvergne
}%, Discrete Mathematics group
\end{center}

\end{frame}

\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.27\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}{\textsc{Bellitto - Bergougnoux}}%\insertauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.4\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\inserttitle
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.33\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
    \usebeamerfont{date in head/foot}\insertshortdate{}\hspace*{2em}
    \insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 
  \end{beamercolorbox}}%
  \vskip0pt%
}


\begin{frame}
 \tableofcontents
\end{frame}

\section{Context}

\subsection{Forbidden transitions}

\begin{frame}
 \frametitle{Motivation}
 
 We use \red{graphs} to model networks in various fields of application (road networks, telecommunication networks, public transit...). 
 
 \vspace{0.35cm}
 \begin{minipage}{0.63\linewidth}
  
  \blue{Set of possible walks} in a road network $\neq$ \red{set of walks in the graph}.
 
 \vspace{0.5cm}
 \textbf{We need a stronger model!}
 \end{minipage}
 \begin{minipage}{0.33\linewidth}
  \includegraphics[scale=0.4]{forbidden.eps}
 \end{minipage}

\end{frame}


\begin{frame}
 \frametitle{Forbidden-transition graphs}
 
\begin{itemize}
 \item \blue{Transition}: pair of adjacent distinct edges.

 \vspace{0.35cm}
 \item \blue{Forbidden-transition graph}: $G=(V,E,T)$ where $T$ is the set of permitted transitions.
 
 \vspace{0.35cm}
 \item The walk $W=(v_1,v_2,\dots,v_k)$ \emph{\blue{uses}} the transition $v_i v_{i+1} v_{i+2}$ for all $i$ such that $v_i\neq v_{i+2}$.
 
 \vspace{0.35cm}
 \item The walk $W$ is \emph{\blue{$T$-compatible}} if and only if it only uses transitions from $T$.
 \end{itemize}

\end{frame}

\begin{frame}
 \frametitle{Related works}
 \begin{itemize}
  \item \blue{Properly-coloured} walks in edge-coloured graphs.
  
  \vspace{0.35cm}
  \item \blue{Antidirected} walks in digraphs.
  
  \vspace{1cm}
  \item \blue{Forbidden} subpaths / subwalks.
 \end{itemize}

\end{frame}


\subsection{Connecting transition sets}

\begin{frame}
 \frametitle{$T$-connectivity}
 
 \begin{block}{$T$-connectivity}
A graph $G=(V,E)$ is \blue{$T$-connected} if and only if there exists a \blue{$T$-compatible walk} between each pair of vertices.

In this case, we say that $T$ is a \blue{connecting transition set} of $G$.
 \end{block}
\end{frame}

\begin{frame}
 
 \[\begin{pspicture}(7.5,2.25)
\psline{-}(0.25,0.5)(1.75,0.5)
\psline{-}(1.75,0.5)(3.25,0.5)
\psline{-}(1,2)(2.5,2)

\psline{-}(1.75,0.5)(1,2)
\psline{-}(1.75,0.5)(2.5,2)

\psline[linecolor=green](1.1,0.1)(2.4,0.1)
\pscurve[linecolor=green](2.32,1.2)(2.13,0.72)(2.6,0.75)
\uncover<2>{\pscurve[linecolor=green](1.18,1.2)(1.37,0.72)(0.9,0.75)}
% \psline[linecolor=red](3.2,-0.1)(3.8,0.5)
% \psline[linecolor=red](3.2,0.5)(3.8,-0.1)

\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](0.25,0.5){0.25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](1,2){0.25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](2.5,2){0.25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](1.75,0.5){0.25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](3.25,0.5){0.25}
\rput(0.25,0.5){$u$}
\rput(1,2){$x$}
\rput(2.5,2){$w$}
\rput(1.75,0.5){$v$}
\rput(3.25,0.5){$y$}
\rput(6,2){$T_1=\{uvy,wvy\}$}
\uncover<2>{\rput(6,1){$T_2=\{uvy,wvy,uvx\}$}}
  \end{pspicture}\]

There exists a \blue{$T_1$-compatible} walk between $w$ and every vertex of the graph.

\vspace{0.35cm}
There exists no \blue{$T_1$-compatible} walk between $x$ and $u$ or $y$.

\vspace{1cm}
\uncover<2>{The set $T_2$ is a \blue{ connecting transition set} of the graph.}
  
\end{frame}

\begin{frame}
 \frametitle{Minimum connecting transition set}
 
 \begin{block}{Minimum connecting transition set}
  Smallest set $T$ such that a graph $G$ is $T$-connected. 
  
  (similar to minimum spanning trees)
 \end{block}
	\vfill
  Several paper studies robustness of graph proprties to malfunctionning transitions.
  
  \vspace{0.3cm}
  $\rightarrow$ Application of minimum connecting transition sets to robust network design ?
 
\end{frame}


\section{Our results}

\begin{frame}
\tableofcontents[currentsection, subsectionstyle=show/show/hide]
\end{frame}


\subsection{Upper bounds}

\begin{frame}
 \frametitle{General case}
 
 Pick a neighbour $n_v$ for every vertex $v$.
 
 \vspace{0.25cm}
 $T=\dcup{v\in V}{} \quad\dcup{u\in N(v)\setminus\{n_v\}}{} \{uvn_v\}$ is connecting.
 
 \vspace{0.25cm}
 $|T|=2|E|-|V|$
 
 \vspace{0.35cm}
 If $G$ is a tree, $|T|=n-2$ and this construction is optimal!
 
 \begin{block}{General upper bound}
  A minimum connecting transition set of a graph of $n$ vertices has size at most $n-2$.
 \end{block}

\end{frame}

\begin{frame}
 \frametitle{Lower bounds and reachable values}
 
 Complete graph: minimum connecting transition set of size 0.
 
 \vspace{0.25cm}
 Every value $k$ between 0 and $n-2$ is reachable: start from a tree of size $k+2$ and add $n-k+2$ dominating vertices.
 
 \vspace{0.35cm}
 \begin{alertblock}{Question}
  Is the size of a minimum connecting transition set $k-2$ where $k$ is the number of non-dominating vertices ?
 \end{alertblock}

\end{frame}

\begin{frame}
 \begin{minipage}{0.47\linewidth}
  \[\begin{pspicture}(4.5,4)
\psline(0.25,1.5)(2.25,0.25)
\psline(0.25,1.5)(2.25,2.25)
\psline(0.25,1.5)(2.25,3.75)
\psline(4.25,1.5)(2.25,0.25)
\psline(4.25,1.5)(2.25,2.25)
\psline(4.25,1.5)(2.25,3.75)
\psline(2.25,2.25)(2.25,3.75)
\psline(0.25,1.5)(4.25,1.5)

\uncover<2->{
\pscurve[linecolor=green](2.15,3)(2.1,3.35)(1.55,2.7)%(1.75,3.05)
% \pscurve[linecolor=green](0.85,2.52)(-0.1,1.5)(0.85,0.88)
\psline[linecolor=green](0.8,0.825)(-0.2,1.45)
\psline[linecolor=green](0.8,2.575)(-0.2,1.45)
}

\pscircle[fillstyle=solid, fillcolor=white](0.25,1.5){0.25}
\pscircle[fillstyle=solid, fillcolor=white](2.25,0.25){0.25}
\pscircle[fillstyle=solid, fillcolor=white](2.25,2.25){0.25}
\pscircle[fillstyle=solid, fillcolor=white](2.25,3.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](4.25,1.5){0.25}

\rput(0.25,1.5){$u_1$}
\rput(2.25,0.25){$u_4$}
\rput(2.25,2.25){$u_3$}
\rput(2.25,3.75){$u_2$}
\rput(4.25,1.5){$u_5$}

\uncover<6->{
\pscircle[fillstyle=solid, fillcolor=red](0.25,1.5){0.25}
\pscircle[fillstyle=solid, fillcolor=blue](2.25,0.25){0.25}
\pscircle[fillstyle=solid, fillcolor=blue](2.25,2.25){0.25}
\pscircle[fillstyle=solid, fillcolor=blue](2.25,3.75){0.25}
\pscircle[fillstyle=solid, fillcolor=yellow](4.25,1.5){0.25}
\rput(0.25,1.5){$u_1$}
\rput(2.25,0.25){\textcolor{white}{$u_4$}}
\rput(2.25,2.25){\textcolor{white}{$u_3$}}
\rput(2.25,3.75){\textcolor{white}{$u_2$}}
\rput(4.25,1.5){$u_5$}
}


\end{pspicture}\]

3 non-dominating vertices.

\uncover<2->{Size of MCTS: 2.}

\end{minipage}
\uncover<3->{
\begin{minipage}{0.47\linewidth}
  \[\begin{pspicture}(5,4)
\psline(0.25,2.75)(2.5,0.25)
\psline(0.25,2.75)(3.25,1)
\psline(0.25,2.75)(2.5,2)
\psline(0.25,2.75)(1.75,3)
\psline(0.25,2.75)(2.5,3.75)
\psline(4.75,1.25)(2.5,0.25)
\psline(4.75,1.25)(3.25,1)
\psline(4.75,1.25)(2.5,2)
\psline(4.75,1.25)(1.75,3)
\psline(4.75,1.25)(2.5,3.75)
\psline(2.5,0.25)(3.25,1)
\psline(3.25,1)(2.5,2)
\psline(2.5,2)(1.75,3)
\psline(1.75,3)(2.5,3.75)

\uncover<4->{
\pscurve[linecolor=green](2.75,0.62)(2.9,1)(2.75,1.5)
\pscurve[linecolor=green](2.25,3.38)(2.1,3)(2.25,2.5)

\pscurve[linecolor=green](2.62,1.5)(2.15,2)(1.88,2.5)

% \pscurve[linecolor=green](1.38,3.5)(2.5,4.1)(3.62,2.75)
\psline[linecolor=green](2.55,4.15)(1.425,3.65)
\psline[linecolor=green](2.55,4.15)(3.675,2.9)
}

\pscircle[fillstyle=solid, fillcolor=white](0.25,2.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](2.5,0.25){0.25}
\pscircle[fillstyle=solid, fillcolor=white](3.25,1){0.25}
\pscircle[fillstyle=solid, fillcolor=white](2.5,2){0.25}
\pscircle[fillstyle=solid, fillcolor=white](1.75,3){0.25}
\pscircle[fillstyle=solid, fillcolor=white](2.5,3.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](4.75,1.25){0.25}

\rput(0.25,2.75){$v_1$}
\rput(2.5,0.25){$v_6$}
\rput(3.25,1){$v_5$}
\rput(2.5,2){$v_4$}
\rput(1.75,3){$v_3$}
\rput(2.5,3.75){$v_2$}
\rput(4.75,1.25){$v_7$}

\uncover<6>{
\pscircle[fillstyle=solid, fillcolor=red](0.25,2.75){0.25}
\pscircle[fillstyle=solid, fillcolor=blue](2.5,0.25){0.25}
\pscircle[fillstyle=solid, fillcolor=blue](3.25,1){0.25}
\pscircle[fillstyle=solid, fillcolor=blue](2.5,2){0.25}
\pscircle[fillstyle=solid, fillcolor=blue](1.75,3){0.25}
\pscircle[fillstyle=solid, fillcolor=blue](2.5,3.75){0.25}
\pscircle[fillstyle=solid, fillcolor=red](4.75,1.25){0.25}
\rput(0.25,2.75){$v_1$}
\rput(2.5,0.25){\textcolor{white}{$v_6$}}
\rput(3.25,1){\textcolor{white}{$v_5$}}
\rput(2.5,2){\textcolor{white}{$v_4$}}
\rput(1.75,3){\textcolor{white}{$v_3$}}
\rput(2.5,3.75){\textcolor{white}{$v_2$}}
\rput(4.75,1.25){$v_7$}
}

\end{pspicture}\]

7 non-dominating vertices.

\uncover<4->{Size of MCTS: 4.}

\end{minipage}
}

\uncover<5->{
\begin{exampleblock}{Key}
 Look at the connected components of the complementary graph!  (co-cc)
\end{exampleblock}
}
\end{frame}

\begin{frame}
 \frametitle{Improved upper bound}
 
 \begin{block}{Theorem}
  Every connected graph $G$ has a connecting transition set of size $\tau(G)$ where

\[\tau(G)=
\dsum{\underset{|C|\geqslant 2}{C\text{ }\mathrm{co}\text{-}\mathrm{cc}\text{ }\mathrm{of}\text{ }G}}{} 
\left\{
\begin{split}
|C|-2&\text{ if }G[C]\text{ is connected}\\
|C|-1&\text{ otherwise }
\end{split}
\right.\]

 \end{block}

\end{frame}



\subsection{Reformulation}

\begin{frame}
 \frametitle{Co-$P_7$}
 
  \[\begin{pspicture}(6.5,1.5)
\psline[linestyle=dashed, linecolor=red](0.25,0.75)(1.25,0.75)
\psline[linestyle=dashed, linecolor=red](1.25,0.75)(2.25,0.75)
\psline[linestyle=dashed, linecolor=red](2.25,0.75)(3.25,0.75)
\psline[linestyle=dashed, linecolor=red](3.25,0.75)(4.25,0.75)
\psline[linestyle=dashed, linecolor=red](4.25,0.75)(5.25,0.75)
\psline[linestyle=dashed, linecolor=red](5.25,0.75)(6.25,0.75)

\only<9>{
\psframe[linecolor=blue](-0.2,-0.2)(3.7,1.7)
\psframe[linecolor=violet](2.9,0.4)(6.6,1.1)
}

\uncover<2->{
\pscurve[linecolor=green](1.25,0.75)(2.25,0.35)(3.05,0.6)
\pscurve[linecolor=green](3.05,0.6)(1.75,-0.05)(0.25,0.75)

}

\uncover<3->{
\pscurve[linecolor=green](2.25,0.75)(1.25,1.15)(0.45,0.9)
\pscurve[linecolor=green](0.45,0.9)(1.75,1.55)(3.25,0.75)
}

\uncover<4->{
\pscurve[linecolor=green](5.25,0.75)(4.25,0.35)(3.45,0.6)
\pscurve[linecolor=green](3.45,0.6)(4.75,-0.05)(6.25,0.75)

\pscurve[linecolor=green](4.25,0.75)(5.25,1.15)(6.05,0.9)
\pscurve[linecolor=green](6.05,0.9)(4.75,1.55)(3.25,0.75)
}

\pscircle[fillstyle=solid, fillcolor=white](0.25,0.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](1.25,0.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](2.25,0.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](3.25,0.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](4.25,0.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](5.25,0.75){0.25}
\pscircle[fillstyle=solid, fillcolor=white](6.25,0.75){0.25}

\rput(0.25,0.75){$v_1$}
\rput(1.25,0.75){$v_2$}
\rput(2.25,0.75){$v_3$}
\rput(3.25,0.75){$v_4$}
\rput(4.25,0.75){$v_5$}
\rput(5.25,0.75){$v_6$}
\rput(6.25,0.75){$v_7$}


\end{pspicture}\]
 
 \uncover<4->{
 Connecting transition of size 4 but $\tau(\overline{P_7})=5$.}
 
 \uncover<5->{
\[\begin{pspicture}(4.8,4)

\psline{-}(0.4,0.4)(1.4,1.4)
\psline{-}(2.4,2.4)(1.4,1.4)
\psline{-}(2.4,2.4)(3.4,1.4)
\psline{-}(3.4,1.4)(4.4,0.4)
\psline{-}(2.4,2.4)(3.4,3.4)
\psline{-}(2.4,2.4)(1.4,3.4)

\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](0.4,0.4){0,25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](1.4,1.4){0,25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](2.4,2.4){0,25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](3.4,1.4){0,25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](4.4,0.4){0,25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](1.4,3.4){0,25}
\pscircle[linecolor=black, fillstyle=solid, fillcolor=white](3.4,3.4){0,25}

% \psline[linecolor=green](2.83,1.41)(4.95,3.54)
% \psline[linecolor=green](7.78,1.41)(5.66,3.54)

% \pscurve[linecolor=green](3.54,4.24)(4.24,5.30)(3.54,6.36)
% \pscurve[linecolor=green](7.07,4.24)(6.36,5.30)(7.07,6.36)

\uncover<6->{
\psline[linecolor=green](1.23,0.57)(2.23,1.57)
\psline[linecolor=green](3.57,0.57)(2.57,1.57)

\pscurve[linecolor=green](1.56,1.9)(1.9,2.4)(1.56,2.9)
\pscurve[linecolor=green](3.24,1.9)(2.9,2.4)(3.22,2.9)
}

\only<6>{
\pscurve[linecolor=green](1.9,3.24)(2.4,2.9)(2.9,3.24)
}

\only<7->{
\pscurve[linecolor=gray, linestyle=dashed](1.9,3.24)(2.4,2.9)(2.9,3.24)
}

\only<8->{
\psframe[linecolor=blue](0.05,-0.1)(2.75,3.9)
\psframe[linecolor=violet](2.1,3.7)(4.7,0.1)
}

\rput(0.4,0.4){$v_3$}
\rput(1.4,1.4){$v_1$}
\rput(2.4,2.4){$v_4$}
\rput(3.4,1.4){$v_7$}
\rput(4.4,0.4){$v_5$}
\rput(1.4,3.4){$v_2$}
\rput(3.4,3.4){$v_6$}

\end{pspicture}\]
}
 
\end{frame}

\begin{frame}
 \frametitle{Optimal connecting hypergraph}
 \begin{block}{Connecting hypergraph:}
 Let $G$ be a graph. A \emph{connecting hypergraph}\index{connecting hypergraph} of $G$ is a set $H$ of subsets of $V(G)$ called \emph{connecting hyperedges}, such that
\begin{itemize}
\item For all $e\in H$, $|e|\geqslant 2$.
\item For all $e\in H$,  $G[e]$ is connected.
\item For all $uv \notin E(G)$, there exists $e\in H$ such that $u,v \in e$ %(we say that the hyperedge $e$ connects $u$ and $v$).
\end{itemize}

% We define the cost of a connecting hypergraph as

\[\mathrm{cost}(H)=\dsum{e\in H}{}(|e| - 2)\]
   \end{block}

   \uncover<2>{Finding a minimum connecting transition set is equivalent to finding connecting hypergraph of minimum cost!}
   
\end{frame}

\subsection{Main results}
\begin{frame}
 \frametitle{Complexity and approximation}
 
 \begin{block}{Complexity} 
 Finding a minimum connecting transition set of a graph is NP-complete.
 \end{block}

%  \vspace{0.35cm}
 \pause
 \begin{block}{Approximation}
 The size of a minimum connecting transition of a graph $G$ is at least $\frac 2 3 \tau(G)$ (tight bound) where
 \[\tau(G)=
\dsum{\underset{|C|\geqslant 2}{C\text{ }\mathrm{co}\text{-}\mathrm{cc}\text{ }\mathrm{of}\text{ }G}}{} 
\left\{
\begin{split}
|C|-2&\text{ if }G[C]\text{ is connected}\\
|C|-1&\text{ otherwise }
\end{split}
\right.\]

We have a $O(|V|^2)$ $\frac 3 2$-approximation!
\end{block}
\end{frame}


\section*{Conclusion}

\begin{frame}
\frametitle{Our results}
\begin{itemize}
 \item Bounds or exact results for several families of graphs.
 
 \vspace{0.2cm}
 \item Reformulation of the problem.
 
 \vspace{0.2cm}
 \item Polynomial $\frac 3 2$-approximation.
 
 \vspace{0.2cm}
 \item Proof of NP-completeness.
\end{itemize}
\end{frame}

\begin{frame}
 \frametitle{Future works}
 \begin{itemize}
  \item Sparse graphs (bounded treewidth ? bounded maximum average degree ? planar ?).
  
  \vspace{0.2cm}
  \item Stretch of the minimum connecting sets.
  
  \vspace{0.2cm}
  \item Starting with forbidden transitions.
  
  \vspace{0.2cm}
  \item Directed graphs.
 \end{itemize}

\end{frame}

\begin{frame}
 \begin{center}\Huge{Thank you!}\end{center}
\end{frame}


\end{document}

